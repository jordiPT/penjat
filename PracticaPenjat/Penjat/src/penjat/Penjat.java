package penjat;
import java.util.Random;
import java.util.ArrayList;
import java.util.Scanner;


public class Penjat {

	public static void main(String[] args) {
	    
		int maxIntents = 11;
		int intentsIncorrectes = 0;
		String paraula = paraulaRandom(); //funcio per a asignar una paraula aleatoria
		
		
		ArrayList<Character> lletresDescobertes = new ArrayList<>(); //creem una llista per a posr les paraules descobertes
		
		System.out.println("Endevina la paraula:");

		boolean paraulaEncertada = false;
		
		while (!paraulaEncertada && intentsIncorrectes < maxIntents)
		{
                    StringBuilder paraulaX = updateParaulaX(paraula,lletresDescobertes);
                    mostraMissatge(intentsIncorrectes, paraula, lletresDescobertes, paraulaX);
			
		    Scanner reader = new Scanner (System.in); //creem objecte scanner
                    char lletra = reader.next(".").charAt(0); //demanem lletra
		     
		     int resultat = comprovaLletra(lletra, paraula, lletresDescobertes);
		     
		     switch(resultat)
		     {
		     	case 1: System.out.println("La lletra esta repetida"); break;
		     	case 2: System.out.println("Correcte la lletra existeix"); break;
		     	case 3: { System.out.println("La lletra no existeix"); intentsIncorrectes++; } break;
		     	default : System.out.print("error");
		     }
		     
		     if(lletresDescobertes.size() == paraula.length())
		     {
		    	 System.out.println("Has endivinat tota la paraula!");
                         paraulaEncertada = true;
		     }
		}
                
                if(paraulaEncertada)         
                {
                    System.out.println("HAS GUANYAT");
                }
                else{
                     System.out.println("S'han acabat tots els intents");
                     System.out.println("HAS PERDUT");
                }

	}
	
	public static String paraulaRandom()
	{
		Random rand = new Random();
		int randomNum = rand.nextInt(3);
		
		String paraules[] = {"cotxe", "roda", "ratoli"};
		
		return paraules[randomNum];
	}
	
	public static StringBuilder updateParaulaX(String paraula, ArrayList<Character> lletresDescobertes)
	{
		StringBuilder p = new StringBuilder("           ");
		for(int x = 0; x< paraula.length(); x++)
		{
			if(lletresDescobertes.contains(paraula.charAt(x)))
			{
                            char word = paraula.charAt(x);    
                            p.setCharAt(x, word);
			}
			else 
			{
				p.setCharAt(x, '*');
			}
		}
		return p;
		
	}
	
	public static void mostraMissatge(int intentsIncorrectes, String paraula, ArrayList<Character> lletresDescobertes, StringBuilder paraulaX)
	{
		System.out.println("Portes " + intentsIncorrectes + " intents incorrectes");
		System.out.println("Has encertat " + lletresDescobertes.size() + " lletres de " + paraula.length() + " lletres que té la paraula");
		System.out.println("Paraula: " + paraulaX);
		
	}
	
	public static int comprovaLletra(char lletra, String paraula, ArrayList<Character> lletresDescobertes)
	{		
		for(int x=0; x < paraula.length(); x++) 
		{
			if (paraula.charAt(x) == lletra) //Si existeix la lletra
			{
                            if(lletresDescobertes.isEmpty())
                            {
                                lletresDescobertes.add(x, lletra);
                                return 2;
                            }
                            for (int y=0; y < lletresDescobertes.size(); y++) 
                            {
                               
                                if (lletresDescobertes.size() <= x) //Si esta repetida
                                {
                                    lletresDescobertes.add(x, lletra);
                                    return 2; 
				}
                                else if (lletresDescobertes.get(x) == lletra) //Si esta repetida
                                {
                                    return 1; 
				}
				else if ( lletresDescobertes.get(x) != lletra) //Si no esta repetida
				{
                                    lletresDescobertes.add(x, lletra);
                                    return 2;
				}
                            }
			}
			/*else if (paraula.charAt(x) != lletra)
			{
                            
                            return 3; //la lletra no existeix
			}	*/
                     if(lletresDescobertes.size() < x)
		     {
		    	 return 3;
		     }
		}
		
		return 0;
	}

}
